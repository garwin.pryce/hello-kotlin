

package demo

import kotlin.coroutines.experimental.suspendCoroutine

fun main(args: Array<String>) {

    val listOfNumber = 1..300

    //sum it up
    println("sum of number is : ${listOfNumber.reduce{ a, x -> a + x}}")

    println("sum of number plus 10: ${listOfNumber.fold(10) { a, x -> a + x}}")

    println("Only evens : ${listOfNumber.filter{ x-> x % 2 == 0}}")
    println("Only odds : ${listOfNumber.filter{it % 2 != 0}}")

    val speicalList = listOfNumber
                        .filter { it % 2 == 0}
                        .map{ it * 10}

    for(item in speicalList) println(item)

    speicalList.forEach { println(it)}
}


fun makeMathFunc(num1: Int ): (Int) -> Int = { num2 -> num1 * num2}
